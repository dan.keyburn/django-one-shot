from django.shortcuts import render
from todos.models import TodoList, TodoItem


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/todo_list_list.html", context)
